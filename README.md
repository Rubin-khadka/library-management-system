# **Library Management System**

## Description
This is just a simple project. It uses c programming to develop library management system

## Main file
-   **.vscode**

    *This file contains run commands of this project*
-   **mainfile**
    -   **library.c**

        *main C file*
    -   **library.h**

        *header file*
    -   **book.c**

        *contains all necessary function like add , delete and edit books*
    -   **members.c**

        *contains all necessary function like add , delete and edit members*    
