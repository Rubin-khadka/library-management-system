/*
    All details of book included here
*/

#include "library.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void handleBookMenu(struct Book *books, int *bookNum){

    int bookOption;
    do{
        printf("\n1. Add Book.\n");
        printf("2. Display Book Details.\n");
        printf("3. Edit Book Details.\n");
        printf("4. Delete book.\n");
        printf("5. Back to Main menu.\n");
        printf("Choice (1-4): ");

        if(scanf("%d", &bookOption) != 1 || bookOption <1 || bookOption > 5){
            printf("Invalid Input!!!!");
            while (getchar() != '\n');
            continue;
        }

        //switch case for book
        switch (bookOption){
        case 1:
            addBook(&books[*bookNum]);
            (*bookNum)++;
            break;

        case 2:
            if (*bookNum > 0){
                printf("Available book information.\n");
                for (int i = 0; i < *bookNum; i++){
                    printf("Book %d \n", i+1);
                    displayBookInfo(&books[i]);
                } 
            }else {
                printf("No book available. \n");
            }
            break;

        case 3: // Edit book
            if (*bookNum > 0){
                editBook(books, bookNum);
            } else{
                printf("No book available to delete.\n");
            }
            break;

        case 4: // delete book
            if (*bookNum > 0){
                deleteBook(books, bookNum);
            } else{
                printf("No book available to delete.\n");
            }
            break;

        case 5:
            printf("Returning back to main menu.\n");
            break;

        default:
            printf("Invalid Choice !!!\n");
            break;
        }
    } while (bookOption != 5);
}

void addBook(struct Book *book){

    printf("Enter Book ID: ");
    scanf("%d", &book->bookID);

    printf("Enter title: ");
    getchar();
    fgets(book->title, sizeof(book->title), stdin);
    book->title[strcspn(book->title, "\n")] = '\0';

    printf("Enter Author: ");
    fgets(book->author, sizeof(book->author), stdin);
    book->author[strcspn(book->author, "\n")] = '\0';

    printf("Enter genre: ");
    fgets(book->genre, sizeof(book->genre), stdin);
    book->genre[strcspn(book->genre, "\n")] = '\0';

    printf("Total number of copies: ");
    scanf("%d", &book->copiesAvailable);
}

void displayBookInfo(struct Book *books){
    printf("BookID: %d \n", books->bookID);
    printf("Title: %s \n", books->title);
    printf("Author: %s \n", books->author);
    printf("Genre: %s \n", books->genre);
    printf("Available Copies: %d \n", books->copiesAvailable);
    printf("\n");
}

void deleteBook(struct Book *books, int *bookNum){
    int bookID;
    printf("Enter the ID of the book to delete: ");
    scanf("%d", &bookID);

    int index = -1;
    for (int i = 0; i < *bookNum; i++){
        if (books[i].bookID == bookID){
            index = i;
            break;
        }    
    }
    if (index != -1){
        for (int i = index; i < *bookNum - 1; i++){
            books[i] = books[i + 1];
        }
        (*bookNum)--;
        printf("Book with ID %d successfully deleted.\n", bookID);
    }else{
        printf("Book with ID %d not found.\n", bookID);
    }
}

void editBook(struct Book *books, int *bookNum){
    int bookID;

    printf("Enter Book ID of book to edit: ");
    scanf("%d", &bookID);

    int index = -1;
    for (int i = 0; i < *bookNum; i++){
        if(books[i].bookID == bookID){
            index = i;
            break;
        }
    }
    if (index != -1){
        printf("Editing Book Details:\n");
        printf("1. Title\n");
        printf("2. Author\n");
        printf("3. Genre\n");
        printf("4. Copies Available\n");
        printf("Enter choice: ");

        int choice;
        scanf("%d", &choice);

            switch (choice) {
            case 1:
                printf("Enter new title: ");
                getchar();
                fgets(books[index].title, sizeof(books[index].title), stdin);
                books[index].title[strcspn(books[index].title, "\n")] = '\0';
                break;
            case 2:
                printf("Enter new author: ");
                getchar();
                fgets(books[index].author, sizeof(books[index].author), stdin);
                books[index].author[strcspn(books[index].author, "\n")] = '\0';
                break;
            case 3:
                printf("Enter new genre: ");
                getchar();
                fgets(books[index].genre, sizeof(books[index].genre), stdin);
                books[index].genre[strcspn(books[index].genre, "\n")] = '\0';
                break;
            case 4:
                printf("Enter new number of copies available: ");
                scanf("%d", &books[index].copiesAvailable);
                break;
            default:
                printf("Invalid choice.\n");
        }
    } else {
        printf("Book with ID %d not found.\n", bookID);
    }
   
}