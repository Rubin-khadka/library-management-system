/*
    main of library management system
*/

#include "library.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){

    int bookNum = 0;
    int memberNum = 0;

    // Dynamic memory allocation
    struct Book *books = (struct Book*) malloc(TOTAL_CAPACITY * sizeof(struct Book));
    if (books == NULL){
        printf("Memory allocation failed. \n");
        // error handling promt TODO
        return 1;
    }

    struct Members *members = (struct Members*) malloc(TOTAL_CAPACITY * sizeof(struct Members));
    if (members == NULL){
        printf("Memory allocation failed. \n");
        return 1;
    }
    
    int choice;
    do{
        printf("\nLibrary Management System.\n");
        printf("1. Books.\n");
        printf("2. Members.\n");
        printf("3. Exit.\n");
        printf("Choice (1 - 3): ");
        
        // Input validation
        if (scanf("%d", &choice) !=1 || choice < 1 || choice > 3 ){
            printf("Invalid input!!");
            while (getchar() != '\n');
            continue;
        }
        
        // Handle user choice
        switch (choice){
            case 1:
                handleBookMenu(books, &bookNum);
                break;
                
            case 2:
                manageMemberMenu(members, &memberNum);
                break;

            case 3:
                printf("Exiting program !!!\n");
                break;

            default:
                printf("Invalid Choice! Please select 1 or 2.\n");
                break;
        }
    } while (choice != 3);

    free(books);
    free(members);
    return 0;
}


