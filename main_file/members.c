/*
    File containing member funstions like add, delete, edit
*/

#include "library.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void manageMemberMenu(struct Members *member, int *memberNum){

    int memberOption = 0;
    do{
        printf("\nMember Management\n");
        printf("1. Add Member.\n");
        printf("2. Display Member detail.\n");
        printf("3. Delete Member.\n");
        printf("4.  Back to main menu.\n");
        printf("Choice (1-4): ");
        scanf("%d", &memberOption);

        switch (memberOption)
        {
        case 1:
            addMembers(&member[*memberNum]);
            (*memberNum)++;
            break;
        
        case 2://Display member
            if (*memberNum > 0){
                displayMembers(member, memberNum);
            } else{
                printf("No members to display.\n");   
            }
            break;

        case 3://delete member
            if(*memberNum > 0){
                deleteMember(member, memberNum);
            } else{
                printf("No members in list.\n");
            }
            break;

        case 4:
            printf("Returning Back to main menu.\n");
            break;    
        
        default:
            printf("Invalid input.\n");
            break;
        }
    } while (memberOption != 4);
    
}

void addMembers(struct Members *member){
    printf("Enter member ID: ");
    scanf("%d", &member->memberID);

    printf("Enter name: ");
    getchar();
    fgets(member->name, sizeof(member->name) , stdin);
    member->name[strcspn(member->name , "\n")] = '\0';
 
    printf("Enter email: ");
    fgets(member->email, sizeof(member->email), stdin);
    member->email[strcspn(member->email, "\n")] = '\0';
    
}

void displayMembers(struct Members *member, int *memberNum){

    for (int i = 0; i < *memberNum; i++)
    {
        printf("Member ID: %d \n", member[i].memberID);
        printf("Member Name: %s \n", member[i].name);
        printf("Member Email: %s \n", member[i].email);
    }

}

void deleteMember(struct Members *member, int *memberNum){

    int memberID;

    printf("Enter ID of member to delete: ");
    scanf("%d", &memberID);

    int index = -1;
    for (int i = 0; i < *memberNum; i++){
        if (member[i].memberID == memberID){
            index = i;
            break;
        }
    }

    if (index != 1){
        for (int i = index; i < *memberNum - 1; i++){
            member[i] = member[i + 1];
            printf("Successfully deleted !!!");
        }
        (*memberNum)--;
    } else{
        printf("Member of ID %d is not found.\n", memberID);
    }
    
}