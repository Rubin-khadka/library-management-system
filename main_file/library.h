/*
    header file of library management system
*/

#ifndef LIBRARY_H
#define LIBRARY_H

#define TOTAL_CAPACITY 100

struct Book{
    int bookID;
    char title[100];
    char author[100];
    char genre[20];
    int copiesAvailable;
};

struct Members
{
    int memberID;
    char name[50];
    char email[100];
};


// functions dealing with books
void handleBookMenu(struct Book *books, int *bookNum);
void addBook(struct Book *book);
void displayBookInfo(struct Book *books);
void deleteBook(struct Book *books, int *bookNum);
void editBook(struct Book *books, int *bookNum);

//functions handling members
void manageMemberMenu(struct Members *member, int *memberNum);
void addMembers(struct Members *member);
void displayMembers(struct Members *member, int *memberNum);
void deleteMember(struct Members *member, int *memberNum);

#endif